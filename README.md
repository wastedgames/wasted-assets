# WASTED-ASSETS


## This Repository contains all assets needed to build the game

Here are the models, music, radio stations, etc..
If you want to add new models/cars/maps just create a PR.
You can learn more about that in the wiki:
https://codeberg.org/wastedgames/wasted-code/wiki/Modders%2FArtists
